#import "ParseStarterProjectViewController.h"
#import <Parse/Parse.h>

@implementation ParseStarterProjectViewController


#pragma mark - UIViewController

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        int tagCount = 0;
        int receivingUserCount = 0;
        int nullUserCount = 0;
    
        PFQuery *query = [PFQuery queryWithClassName:@"NewMarcoPolo"];
        [query orderByDescending:@"createdAt"];
        [query includeKey:@"sendingUser"];
        [query includeKey:@"receivingUsers"];
        [query includeKey:@"hiddenUsers"];
        [query includeKey:@"likedUsers"];
        [query setLimit:1000];
        
        const int thousandsOfTags = 1; //must be between 1 and 10
        
        for(int i = 0; i < thousandsOfTags; i++) {
            
            NSLog(@"i: %d", i);
            NSError *error = nil;
            
            [query setSkip:(i*1000)];
            
            NSArray *objects = [query findObjects:&error];
            if(error) {
                NSLog(@"ERROR querying for tags: %@", error);
                return;
            }
            
            for(PFObject *obj in objects) {
                
                tagCount++;
                
                NSArray *receivingUsers = [obj objectForKey:@"receivingUsers"];
                for(PFUser *user in receivingUsers) {
                    
                    receivingUserCount++;
                    
                    if([user isEqual:((PFUser *)[NSNull null])]) {
                        nullUserCount++;
                        NSLog(@"NULL VALUE: %@", obj.objectId);
                    }
                }
                
            }
        }
        
        NSLog(@"STATISTICS:");
        NSLog(@"tags: %d", tagCount);
        NSLog(@"receiving users: %d", receivingUserCount);
        NSLog(@"null users: %d", nullUserCount);
        NSLog(@"null %%: %f", ((float)nullUserCount/(float)receivingUserCount) * 100);
    });
    
    
    
}

@end
